#include <vector>
#include <string>
#include <fstream>
#include <cstdlib>
#include <thread>
#include <mutex>

#ifdef WINDOWS
#include <ncursesw/ncurses.h>
#define KEY_BACKSPACE 8
#define KEY_ENTER 10
#else
#include <ncurses.h>
#endif

//#define DEBUG

std::mutex m_writeToConsole;

struct Score {
    std::string nick;
    int score;
    int hintsUsed;
};

int menu(std::vector<std::string> choices, WINDOW *win) {
    int choice = 0;
    int y = getcury(win);
    while (1) {
        m_writeToConsole.lock();
        wmove(win, y, 0);
        for (int i = 0; i < choices.size(); ++i) {
            if (i == choice) {
                wattron(win, A_STANDOUT);
            }
            wprintw(win, choices[i].c_str());
            if (i == choice) {
                wattroff(win, A_STANDOUT);
            }
            wprintw(win, "\n");
        }
        wrefresh(win);
        m_writeToConsole.unlock();
        switch (wgetch(win)) {
            case KEY_UP:
                --choice;
                break;
            case KEY_DOWN:
                ++choice;
                break;
            case '\n':
                return choice;
        }
        if (choice < 0) {
            choice = 0;
        } else if (choice > choices.size() - 1) {
            choice = choices.size() - 1;
        }
    }
}

int random(int min, int max) {
    return min + rand() % (( max + 1 ) - min);
}

void game();
void howTo();
void showLeaderboard();

int main() {
    srand(time(NULL));
    setlocale(LC_CTYPE,"C-UTF-8");
    initscr();
    cbreak();
    keypad(stdscr, 1);
    std::vector<std::string> choices = {
        "Graj",
        "Wyniki",
        "Jak grac?",
        "Napisy",
        "Wyjdz"
    };
    while (1) {
        clear();
        printw("przyslowia (bez polskiego \"l\" i mala litera)\n\
by Grzesiek11\n\
Na licensji GNU GPL v3.0\n\
GitLab: https://gitlab.com/grzesiek11/przyslowia\n\n");
        switch (menu(choices, stdscr)) {
            case 0:
                game();
                break;
            case 1:
                showLeaderboard();
                break;
            case 2:
                howTo();
                break;
            case 3:
                clear();
                printw("Grzesiek11 - projektowanie, programowanie, szata \"graficzna\"\n\
emka7 - kilka fajnych pomyslow (wiekszosc ostatecznie wyrzucona ze wzgledu na deadline), \
testy\n\
Mama - testy");
                getch();
                break;
            case 4:
                endwin();
                return 0;
        }
    }
}

void showLeaderboard() {
    clear();
    std::ifstream f_leaderboard("leaderboard.txt");
    if (!f_leaderboard.is_open()) {
        printw("Nie znaleziono pliku z wynikami. Byc moze nie grales jeszcze ani razu?\n");
        getch();
        return;
    }
    std::vector<Score> scores;
    while (1) {
        Score score;
        if (!(f_leaderboard >> score.nick)) {
            break;
        }
        f_leaderboard >> score.score;
        f_leaderboard >> score.hintsUsed;
        scores.push_back(score);
    }
    printw("Imie Wynik Podpowiedzi\n");
    for (Score score: scores) {
        printw("%s %i %i\n", score.nick.c_str(), score.score, score.hintsUsed);
    }
    getch();
}

void howTo() {
    std::vector<std::string> message = {
        "Gra polega na odgadywaniu zakrytych czesci przyslow. Wyrazy te nalezy wpisac \
na klawiaturze - masz na to 20 sekund.",
        "Jesli zrobisz to poprawnie, otrzymujesz punkt. Jesli nie, zegar nadal tyka \
- mozesz wprowadzic poprawke uzywajac Backspace.",
        "Gdy czas uplynie, a ty nie udzielisz odpowiedzi - tracisz jedno zycie. Masz \
ich 5.",
        "Jesli nie znasz jakiegos przyslowia - nie martw sie! Mozesz kupic podpowiedz \
za 10 punktow uzywajac Taba. Polega ona na pokazaniu poprawnego fragmentu - musisz \
jednak go wpisac.",
        "Jesli nie masz 10 punktow do wydania, w ramach pocieszenia \
dostaniesz cale przyslowie do przeczytania - nastepnym razem bedziesz \
juz wiedzial!",
        "Nie ma konkretnego celu - myslalem nad tym, ale nie wymyslilem nic mozliwego do \
zrobienia w 2 dni, wiec jest to po prostu zdobywanie rekordow i konkurowanie z innymi.",
        "Uprzedzajac pytania - tak, obsluga jest mozliwa tylko za pomoca klawiatury. Co ja \
poradze ze kocham terminal? :D",
        "Baza przyslow jest spora - prawie 1000! Niestety, mozliwe ze jest w niej pare \
bledow... Jesli jakies znajdziesz, zglos je na GitLabie lub na stronie jamu.",
        "Sama gra rowniez ma kilka problemow - dolaczylem na krotko przed koncem jamu, \
wiec nie jest super dopracowana. Miala byc duzo ladniejsza (choc wciaz utrzymana w stylistyce \
terminala), miec ustawienia trudnosci, wsparcie Unicode (a co za tym idzie - polskich \
znakow)...",
        "...ale nie starczylo czasu. Byc moze bede ja jeszcze rozwijal poza \"dzemem\". Bugi - \
na GitLaba.",
        "W kazdym razie, baw sie dobrze!"
    };
    for (std::string part: message) {
        clear();
        printw(part.c_str());
        getch();
    }
}

void timer(WINDOW *timerWin, int seconds, bool &isUp, bool &end);
bool isWritableCh(char ch);

void game() {
    const int maxHp = 5;
    int hp = 5;
    int points = 0;
    int usedHints = 0;

    WINDOW *levelWin = newwin(1, 80 - maxHp, 0, 0);
    WINDOW *hpWin = newwin(1, maxHp, 0, 80 - maxHp);
    WINDOW *gameWin = newwin(22, 80, 1, 0);
    keypad(gameWin, 1);
    nodelay(gameWin, 1);
    WINDOW *timerWin = newwin(1, 80, 23, 0);
    std::ifstream f_list("list.txt");
    if (!f_list.is_open()) {
        clear();
        printw("Can't load list.txt.");
        getch();
        exit(-1);
    }
    std::vector<std::string> list;
    {
        std::string tmp;
        while (std::getline(f_list, tmp)) {
            list.push_back(tmp);
        }
    }
    while (hp > 0) {
        std::string input;
        std::string randomProverb = list[random(0, list.size() - 1)].c_str();
        std::string tmp = "";
        bool hint = 0;
        std::vector<std::string> splittedProverb;
        std::vector<int> words;
        for (char ch: randomProverb) {
            if (isWritableCh(ch)) {
                tmp += ch;
            } else {
                if (tmp != "") {
                    splittedProverb.push_back(tmp);
                    words.push_back(splittedProverb.size() - 1);
                    tmp = "";
                }
                splittedProverb.push_back(std::string(1, ch));
            }
        }
        int removedWord = words[random(0, words.size() - 1)];
        bool isUp = 0, end = 0;
        std::thread timerThread(timer, timerWin, 20, std::ref(isUp), std::ref(end)); 
        wclear(levelWin);
        wprintw(levelWin, "%i", points);
        wrefresh(levelWin);
        wclear(hpWin);
        wprintw(hpWin, "%i", hp);
        while (input != splittedProverb[removedWord] && !isUp) {
            m_writeToConsole.lock();
            wrefresh(hpWin);
            wclear(gameWin);
            for (int i = 0; i < splittedProverb.size(); ++i) {
                if (i != removedWord) {
                    wprintw(gameWin, splittedProverb[i].c_str());
                } else {
                    wprintw(gameWin, "%s%s",
                    input.c_str(), std::string(splittedProverb[removedWord].size() - input.size(), '_').c_str());
                }
            }
            if (points >= 10 && !hint) {
                wprintw(gameWin, "\n\nPodpowiedz za 10 punktow? Nacisnij Tab!");
            } else if (hint) {
                wprintw(gameWin, "\n\nPodpowiedz: %s", splittedProverb[removedWord].c_str());
            }
            m_writeToConsole.unlock();
            int ch = -1;
            while (ch == ERR && !isUp) {
                ch = wgetch(gameWin);
            }
            switch (ch) {
                case KEY_BACKSPACE:
                    if (input.size() > 0) {
                        input.pop_back();
                    }
                    break;
                #ifdef DEBUG
                case KEY_RIGHT:
                    input = splittedProverb[removedWord];
                    break;
                case KEY_LEFT:
                    isUp = 1;
                    break;
                #endif
                case '\t':
                    if (points >= 10) {
                        points -= 10;
                        hint = 1;
                        ++usedHints;
                    }
                    break;
                default:
                    if (input.size() < splittedProverb[removedWord].size() && isWritableCh(ch)) {
                        input += ch;
                    }
                    break;
            }
        }
        end = 1;
        timerThread.join();
        wclear(gameWin);
        if (input == splittedProverb[removedWord]) {
            ++points;
            wprintw(gameWin, "%s\nSwietnie!", randomProverb.c_str());
        } else if (isUp) {
            --hp;
            wprintw(gameWin, "Niestety, poprawne przyslowie to:\n%s", randomProverb.c_str());
        }
        wprintw(gameWin, "\nNacisnij dowolny klawisz...");
        wrefresh(gameWin);
        isUp = 0;
        end = 0;
        std::thread timerThread2(timer, timerWin, 10, std::ref(isUp), std::ref(end));
        while (wgetch(gameWin) == ERR && !isUp) {}
        end = 1;
        timerThread2.join();
    }
    keypad(stdscr, 1);
    std::string nick;
    while (1) {
        clear();
        printw("KONIEC GRY\n\n\
    Zdobyles %i punktow, uzywajac %i podpowiedzi\n", points, usedHints);
        printw("Jak sie nazywasz? %s", nick.c_str());
        int ch = getch();
        if (isWritableCh(ch)) {
            nick.push_back(ch);
        } else if (ch == KEY_ENTER) {
            break;
        } else if (ch == KEY_BACKSPACE) {
            nick.pop_back();
        }
    }
    std::ofstream f_leaderboard("leaderboard.txt", std::ios::app);
    if (!f_leaderboard.is_open()) {
        clear();
        printw("Can't open leaderboard.txt.");
        getch();
    } else {
        f_leaderboard << nick << '\n' << points << '\n' << usedHints << '\n';
    }
}

void timer(WINDOW *timerWin, int seconds, bool &isUp, bool &end) {
    const int timerEnd = time(NULL) + seconds;
    int currentOld = -1;
    while (!(isUp || end)) {
        int current = timerEnd - time(NULL);
        if (current != currentOld) {
            m_writeToConsole.lock();
            currentOld = current;
            wclear(timerWin);
            for (int i = 0; i < current; ++i) {
                for (int j = 0; j < 80 / seconds; ++j) {
                    if (i == current - 1 && j == 80 / seconds - 1) {
                        waddch(timerWin, '>');
                    } else {
                        waddch(timerWin, '=');
                    }
                }
            }
            if (current <= 0) {
                isUp = 1;
            }
            wrefresh(timerWin);
            m_writeToConsole.unlock();
        }
    }
}

bool isWritableCh(char ch) {
    return ((ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122) || (ch >= 48 && ch <= 57));
}