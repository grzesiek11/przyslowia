all:
	g++ main.cpp -lncursesw -lpthread -o przyslowia
release:
	g++ main.cpp -O3 -static -lrt -lncursesw -Wl,--whole-archive -lpthread -Wl,--no-whole-archive -ltinfo -o przyslowia
windows-pdcurses_release:
	g++ main.cpp -DWINDOWS -O3 -Wl,-Bdynamic -lpdcurses -static -Wl,--whole-archive -lpthread -Wl,--no-whole-archive -o przyslowia.exe
windows-ncurses_release:
	g++ main.cpp -DWINDOWS -O3 -static -lncursesw -Wl,--whole-archive -lpthread -Wl,--no-whole-archive -o przyslowia.exe